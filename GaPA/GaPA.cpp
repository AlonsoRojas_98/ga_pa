#include <iostream>
#include <Windows.h>
#include <list>
#include <thread>

struct Color {
    float r = 0;
    float g = 0;
    float b = 0;
};

Color hexToRGB(COLORREF colorIn) {
    Color result;
    result.r = (colorIn & 0xff) / 255.0;
    result.g = ((colorIn >> 8) & 0xff) / 255.0;
    result.b = ((colorIn >> 16) & 0xff) / 255.0;

    return result;
}

bool buscarMob(HDC dc) {
    
    float DX = 33.25;
    float DY = 16.6956;

    float startX = 992.0;
    float startY = 399.0;

    int black_battle_x = 1400;
    int black_battle_y = 280;

        
    float pixelPosX = startX + DX;
    float pixelPosY = startY + 434.0856 - 16.6956;


    std::cout << "buscando mob..." << "\n";
    bool filaPar = true;

    for (size_t j = 0; j < 26; j++)
    {
        for (size_t i = 0; i < 13; i++)
        {
            if (i == 9 && (j == 3 || j == 5)) {
                pixelPosX += DX * 2.0;
                continue;
            }
            if ((i == 9 || i == 8) && j == 4) {
                pixelPosX += DX * 2.0;
                continue;
            }
            if (i == 6 && j == 13) {
                pixelPosX += DX * 2.0;
                continue;
            }

            COLORREF color_previo = GetPixel(dc, pixelPosX, pixelPosY - 20);
            Color test_previo = hexToRGB(color_previo);
            Sleep(10);
            SetCursorPos(pixelPosX, pixelPosY);            
            Sleep(50);           

            COLORREF color = GetPixel(dc, pixelPosX , pixelPosY - 20);
            Color test = hexToRGB(color);
            if (test.r != test_previo.r) {
                std::cout << "mob at " << pixelPosX << ", " << pixelPosY << std::endl;
                std::cout << "i: " << i << ", j: " << j << std::endl;
                mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
                Sleep(100);
                SetCursorPos(black_battle_x, black_battle_y);
                mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
                Sleep(5000);

                COLORREF color2 = GetPixel(dc, black_battle_x, black_battle_y);
                Color test2 = hexToRGB(color2);

                if (test2.r == 0 && test2.g ==0 && test2.b ==0) {
                    std::cout << "batalla\n";
                    return true;
                }
            }
            pixelPosX += DX * 2.0;

        }
        filaPar = !filaPar;
        pixelPosX = startX + DX;
        if (!filaPar) pixelPosX -= DX;
        pixelPosY -= DY;
    }
    
    return false;

}

void battalla(HDC dc) {
    float enemigo_1_x = 1840;
    float enemigo_2_x = 1800;
    float enemigo_3_x = 1750;
    float enemigo_1_y = 210;

    int black_battle_x = 1400;
    int black_battle_y = 280;

    INPUT space[2] = {};
    space[0].type = INPUT_KEYBOARD;
    space[0].ki.wVk = VK_SPACE;
    space[1].type = INPUT_KEYBOARD;
    space[1].ki.wVk = VK_SPACE;
    space[1].ki.dwFlags = KEYEVENTF_KEYUP;

    INPUT esc[2] = {};
    esc[0].type = INPUT_KEYBOARD;
    esc[0].ki.wVk = VK_ESCAPE;
    esc[1].type = INPUT_KEYBOARD;
    esc[1].ki.wVk = VK_ESCAPE;
    esc[1].ki.dwFlags = KEYEVENTF_KEYUP;


    INPUT num2[2] = {};
    num2[0].type = INPUT_KEYBOARD;
    num2[0].ki.wVk = VK_NUMPAD2;
    num2[1].type = INPUT_KEYBOARD;
    num2[1].ki.wVk = VK_NUMPAD2;
    num2[1].ki.dwFlags = KEYEVENTF_KEYUP;


    Sleep(1000);
    //start combat (space)
    SendInput(ARRAYSIZE(space), space, sizeof(INPUT));

    while (true)
    {
        COLORREF color2 = GetPixel(dc, black_battle_x, black_battle_y);
        Color test2 = hexToRGB(color2);
        if (test2.r != 0 || test2.g != 0 || test2.b != 0) {
            break;
        }

        Sleep(2000);

        //click enemigo 1
        SetCursorPos(enemigo_1_x, enemigo_1_y);
        SendInput(ARRAYSIZE(num2), num2, sizeof(INPUT));
        Sleep(200);
        mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
        Sleep(600);

        //click enemigo 2
        SetCursorPos(enemigo_2_x, enemigo_1_y);
        SendInput(ARRAYSIZE(num2), num2, sizeof(INPUT));
        Sleep(200);
        mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
        Sleep(600);

        //click enemigo 3
        SetCursorPos(enemigo_3_x, enemigo_1_y);
        SendInput(ARRAYSIZE(num2), num2, sizeof(INPUT));
        Sleep(200);
        mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
        Sleep(600);

        //pasar (space)
        SendInput(ARRAYSIZE(space), space, sizeof(INPUT));

        Sleep(2000);
    }

    //esperar fin combate y cerrar menu
    Sleep(1000);
    SendInput(ARRAYSIZE(esc), esc, sizeof(INPUT));
}

int main()
{    

    HDC dc = GetDC(NULL);
    int x = 0;

    Sleep(3000);

    while (true)
    {

        Sleep(100);
        system("CLS");

        //get mouse pos


        POINT p;
        if (GetCursorPos(&p))
        {
            std::cout << p.x << ", " << p.y << "\n";
        }

        if (buscarMob(dc)) battalla(dc);
    }

    ReleaseDC(NULL, dc);
}